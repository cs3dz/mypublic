﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance = null;
    //  Chamar assim: GameManager.Instance.Metodo();

    [SerializeField]
    public Transform _spawnPointLaunch;
    public Transform SpawnpointLauch { get { return _spawnPointLaunch; } }

    [SerializeField]
    public List<GameObject> _listBalls;

    [SerializeField]
    private int _ballCounter = 1;
    public int BallCounter { get { return _ballCounter; } set { _ballCounter = value; } }

    [SerializeField]
    private Transform _launchPoint;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            if (Instance != this)
            Destroy(gameObject);
    }


    public void EnableLaunch()
    {
        foreach(GameObject ball in _listBalls)
        {
            if (!ball.GetComponent<Ball>().Active)
            {
                ball.transform.position = _launchPoint.position;
                ball.SetActive(true);
                ball.GetComponent<Ball>().Active = true;
                ball.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
                break;
            }
        }
    }


}
