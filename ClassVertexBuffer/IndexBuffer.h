#pragma once

#include <GL/glew.h>

class IndexBuffer
{
private:
	unsigned int _ibo;
	unsigned int _count;

public:
	IndexBuffer(unsigned int count, const unsigned int *data);
	~IndexBuffer();

	void Bind();
	void Unbind();

	inline unsigned int GetCount() { return _count; }
};
