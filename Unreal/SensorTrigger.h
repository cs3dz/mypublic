// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerBox.h"
#include "SensorTrigger.generated.h"

/**
 * 
 */
UCLASS()
class UNREALSTART_API ASensorTrigger : public ATriggerBox
{
	GENERATED_BODY()
	
protected:
	virtual void BeginPlay();

public:
	ASensorTrigger();

	UFUNCTION()
		void OnOverlapBegin(class AActor *OverlappedActor, class AActor* OtherActor);

	UFUNCTION()
		void OnOverlapEnd(class AActor *OverlappedActor, class AActor* OtherActor);

	UPROPERTY(EditAnywhere)
		class AActor *  specifcActor;

};
