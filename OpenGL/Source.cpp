#include <stdio.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <string>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include <vector>

#include "Mesh.h"

std::vector<Mesh*> meshList;

const GLint WIDTH = 800, HEIGHT = 600;

GLuint shader, uniformModel, uniformProjection;
float moveX = 0.0f;
bool dir = true;
float increment = 0.01;

float toRadians = 3.14159265f / 180.0f;
float angle = 0.0f;

// vertex array
static const char* VShader = 
"									\n\
#version 460						\n\
layout(location = 0) in vec3 pos;	\n\
uniform mat4 model;					\n\
uniform mat4 projection;			\n\
out vec4 vCol;						\n\
									\n\
void main()							\n\
{									\n\
									\n\
	gl_Position = projection * model * vec4(pos, 1.0f);	\n\
	vCol = vec4(clamp(pos, 0.0f, 1.0f), 1.0f);	\n\
									\n\
}";


static const char* FShader =
"									\n\
#version 460						\n\
out vec4 color;						\n\
in vec4 vCol;						\n\
									\n\
void main()							\n\
{									\n\
	color = vCol;	\n\
}";





void CreateTriangle()
{

	GLint indices[] =
	{
		0,1,2,
		0,3,2,
		1,3,2,
		0,3,1
	};


	GLfloat vertices[] =
	{
		-1.0,	-1.0,	0.0,
		 1.0,	-1.0,	0.0,
		 0.0,	1.0,	0.0,
		 0.0,	-1.0,	1.0
	};

	Mesh* obj1 = new Mesh();
	obj1->CreateMesh(vertices, indices, 12, 12);
	meshList.push_back(obj1);

}

void AddShader(GLuint theProgram, const char* shaderCode, GLenum shaderType)
{
	GLuint theShader = glCreateShader(shaderType);

	const char* theCode[1];
	theCode[0] = shaderCode;
	GLint codeLength[1];

	codeLength[0] = strlen(shaderCode);

	glShaderSource(theShader, 1, theCode, codeLength);
	glCompileShader(theShader);

	GLint result = 0;
	GLchar eLog[1024] = { 0 };

	glGetShaderiv(theShader, GL_COMPILE_STATUS, &result);

	if (!result)
	{
		glGetProgramInfoLog(shader, sizeof(eLog), NULL, eLog);
		printf("ERROR COMPILING: %s", eLog);
		return;
	}

	glAttachShader(theProgram, theShader);
}



void CompileShaders()
{
	shader = glCreateProgram();

	if (!shader)
	{
		printf("Error creating shader \n");
		return;
	}


	AddShader(shader, VShader, GL_VERTEX_SHADER);
	AddShader(shader, FShader, GL_FRAGMENT_SHADER);

	GLint result = 0;
	GLchar eLog[1024] = { 0 };

	glLinkProgram(shader);

	glGetProgramiv(shader, GL_LINK_STATUS, &result);
	if (!result)
	{
		glGetProgramInfoLog(shader, sizeof(eLog), NULL, eLog);
		printf("ERROR LINKING: %s", eLog);
		return;
	}

	glValidateProgram(shader);
	
	glGetProgramiv(shader, GL_VALIDATE_STATUS, &result);
	if (!result)
	{
		glGetProgramInfoLog(shader, sizeof(eLog), NULL, eLog);
		printf("Error validating %s", eLog);
		return;
	}

	uniformModel = glGetUniformLocation(shader, "model");
	uniformProjection = glGetUniformLocation(shader, "projection");

}






int main()
{
#pragma region GLFW

	if (!glfwInit())
	{
		printf("GLFW inicializacao falhou");
		glfwTerminate();
		return 1;
	}

	// setup window
	// OpenGL version
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR,4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
	// PROFILE
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	// Create window
	GLFWwindow *mainWindow = glfwCreateWindow(WIDTH, HEIGHT,"titulo" ,NULL, NULL);

	if (!mainWindow)
	{
		printf("glfw window creation failed!");
		glfwTerminate();
		return 1;
	}

	// Get buffer size
	int bufferWidth, bufferHeight;
	glfwGetFramebufferSize(mainWindow, &bufferWidth, &bufferHeight);
	printf("%d %d", bufferWidth, bufferHeight);

	// Set context to glew
	glfwMakeContextCurrent(mainWindow);

	// allow modern extension features
	glewExperimental = GL_TRUE;

#pragma endregion


# pragma region GLEW

	if (glewInit() != GLEW_OK)
	{
		printf("GLEW initialization failed!");
		// agora destruir janela q foi contruida com glfw
		glfwDestroyWindow(mainWindow);
		glfwTerminate();
		return 1;
	}
#pragma endregion


	// GL ENABLE
	glEnable(GL_DEPTH_TEST);

	// Setup viewport size
	// usar o que pegar do framebuffer e nao de WITH usado pra criar a janela
	glViewport(0, 0, bufferWidth, bufferHeight);


	CreateTriangle();
	CompileShaders();

	//glm::mat4 model = glm::mat4(1.0f);
	glm::mat4 projection = glm::perspective(45.0f, (GLfloat)bufferWidth/(GLfloat)bufferHeight, 0.1f, 100.0f);
	glUniformMatrix4fv(uniformProjection, 1, GL_FALSE, glm::value_ptr(projection));



 #pragma region LOOP

	// loop until window close
	while (!glfwWindowShouldClose(mainWindow))
	{
		// get * handle events
		glfwPollEvents();

		// clear window
		glClearColor(0.0f, 1.0f, 1.0f,1.0f);
		glClear(GL_COLOR_BUFFER_BIT| GL_DEPTH_BUFFER_BIT);


		if ((moveX > 1.0f) || (moveX < -1.0f))
			if (moveX > 1.0f)
				dir = false;
			else
				dir = true;
		
		// moveX
		if (dir)
			moveX += increment;
		else
			moveX -= increment;

		angle += 1.0f;
		if (angle >= 360.0f)
			angle -= 360.0f;

		// Usando shader
		glUseProgram(shader);

		// Matrix model
		glm::mat4 model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(moveX, 0.0f, -2.5f));
		//model = glm::rotate(model, angle * toRadians, glm::vec3(1.0f, 0.1f, 0.0f));
		model = glm::scale(model, glm::vec3(0.5f, 0.5f, 1.0f));

		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		glUniformMatrix4fv(uniformProjection, 1, GL_FALSE, glm::value_ptr(projection));

		// Render Objects
		meshList[0]->RenderMesh();

		glUseProgram(0);

		glfwSwapBuffers(mainWindow);
	}

#pragma endregion

	return 0;
}