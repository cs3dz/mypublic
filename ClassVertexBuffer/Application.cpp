#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <fstream>
#include <sstream>
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include "VertexBuffer.h"
#include "IndexBuffer.h"

struct ShaderProgramSource
{
	std::string vertexSource;
	std::string fragmentSource;
};


static ShaderProgramSource ParseShader(const std::string & filepath)
{
	std::ifstream stream(filepath);
	std::string line;
	std::stringstream ss[2];

	enum class ShaderType
	{
		NONE = -1, VERTEX = 0, FRAGMENT = 1
	};
	ShaderType type = ShaderType::NONE;

	while (getline(stream, line))
	{
		if (line.find("#shader") != std::string::npos)
		{
			if (line.find("vertex") != std::string::npos)
			{
				type = ShaderType::VERTEX;
			}
			else if (line.find("fragment") != std::string::npos)
			{
				type = ShaderType::FRAGMENT;
			}
		}
		else // se nao for linha que indica tipo de shader escreve a linha no shader atual
		{
			ss[(int)type] << line << "\n";
		}
	}

	return { ss[0].str(), ss[1].str() };
}

static unsigned int CompileShader(unsigned int type, const std::string& source)
{
	unsigned int id = glCreateShader(type);
	const char *src = source.c_str();
	glShaderSource(id, 1, &src, nullptr);
	glCompileShader(id);

	int result;
	glGetShaderiv(id, GL_COMPILE_STATUS, &result);
	if (result == GL_FALSE)
	{
		int length;
		glGetShaderiv(id, GL_INFO_LOG_LENGTH, &length);
		char *message = (char*)alloca(length * sizeof(char));
		glGetShaderInfoLog(id, length, &length, message);
		std::cout << "falhou" << std::endl;
		std::cout << message << std::endl;
		return 0;
	}

	return id;
}


static unsigned int CreateShader(const std::string& vertexShader, const std::string& fragmentShader)
{
	unsigned int program = glCreateProgram();
	unsigned int vs = CompileShader(GL_VERTEX_SHADER, vertexShader);
	unsigned int fs = CompileShader(GL_FRAGMENT_SHADER, fragmentShader);

	glAttachShader(program, vs);
	glAttachShader(program, fs);
	glLinkProgram(program);
	glValidateProgram(program);

	glDeleteShader(vs);
	glDeleteShader(fs);

	return program;
}





int main(void)
{
	GLFWwindow* window;

	/* Initialize the library */ 
	if (!glfwInit())
		return -1;

	/* Create a windowed mode window and its OpenGL context */
	window = glfwCreateWindow(640, 480, "Hello World", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		return -1;
	}

	/* Make the window's context current */
	glfwMakeContextCurrent(window);


	GLenum err = glewInit();
	if (GLEW_OK != err)
	{
		/* Problem: glewInit failed, something is seriously wrong. */
		
	}


	// =================  INDEX BUFFER
	// MODELO 3D
	float vertex_data[] = {
						    -0.5,	-0.5, 0.0, 0.0,
							 0.5,	-0.5, 1.0, 0.0,
							 0.5,	 0.5, 1.0, 1.0,
						    -0.5,	 0.5, 0.0, 1.0
	};

	// Indices
	unsigned int indices[] = {
		0, 3, 2,
		0, 2, 1
	};

	// VERTEX ARRAY OBJECT
	unsigned int vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// VERTEX BUFFER
	/*
	unsigned int vBuffer;
	glGenBuffers(1, &vBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vBuffer);
	glBufferData(GL_ARRAY_BUFFER, 4 * 4 * sizeof(float), vertex_data, GL_STATIC_DRAW);
	*/
	VertexBuffer buf(4 * 4 * sizeof(float), vertex_data);


	// positions
	unsigned int attr_position = 0;
	glEnableVertexAttribArray(attr_position);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 4, 0);

	// texCoord
	unsigned int attr_textCoord = 1;
	glEnableVertexAttribArray(attr_textCoord);
	glVertexAttribPointer(attr_textCoord, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 4, (void*)(2 * sizeof(float)));

	// INDEX BUFFER
	/*
	unsigned int ibo;
	glGenBuffers(1, &ibo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * sizeof(unsigned int), indices, GL_STATIC_DRAW);
	*/
	IndexBuffer ib(6, indices);





	// TEXTURE **************************************
	glEnable(GL_TEXTURE_2D);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	unsigned int m_RendererID;

	std::string m_filePath = "textures/skull.png";
	unsigned char* m_localBuffer;
	int m_width = 240, m_height = 240, m_BPP = 8;
	stbi_set_flip_vertically_on_load(1);
	m_localBuffer = stbi_load(m_filePath.c_str(), &m_width, &m_height, &m_BPP, 4);   // CARREGA TEXTURA  4 = rgba
	if (m_localBuffer == NULL)
	{
		std::cout << stbi_failure_reason() << std::endl;
	}

	glGenTextures(1, &m_RendererID);
	glActiveTexture(GL_TEXTURE1);  // SLOT n enviar para o shader
	glBindTexture(GL_TEXTURE_2D, m_RendererID);
	// Resampling para aumentar ou diminuir imagem. Se nao colocar essas fun�oes a imagem fica preta
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_width, m_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, m_localBuffer);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, m_RendererID);

	std::string vertexShader =
		"#version 430 core \n"
		"layout(location = 0) in vec4 position; \n"
		"layout(location = 1) in vec2 texCoord; \n"

		"out vec2 v_TexCoord;"

		" void main()\n"
		"{ \n"
		" gl_Position = position; \n"
		" v_TexCoord = texCoord;"

		"} \n";


	std::string fragmentShader =
		"#version 430 core \n"
		"layout(location = 0) out vec4 color; \n"

		"in vec2 v_TexCoord;"

		"uniform vec4 u_Color; \n"
		"uniform sampler2D u_texture;  "


		" void main()\n"
		"{ \n"
		"	vec4 texColor = texture(u_texture, v_TexCoord);"
		"	color = texColor; \n"
		"} \n";





	// SHADER
	unsigned int shader = CreateShader(vertexShader, fragmentShader);
	glUseProgram(shader);

	

	//UNIFORM
	int location = glGetUniformLocation(shader, "u_Color");
	glUniform4f(location, 1, 1, 1, 1);

	int texLoc = glGetUniformLocation(shader, "u_texture");
	glUniform1i(texLoc, 1);


	int attrib_location = glGetAttribLocation(shader, "texCoord");
	std::cout << "LOCATION " << attrib_location << std::endl;

	// UNBIND
	glUseProgram(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);


	/* Loop until the user closes the window */
	while (!glfwWindowShouldClose(window))
	{
		/* Render here */
		glClear(GL_COLOR_BUFFER_BIT);
		glClearColor(0.13f, 0.1f, 0.2f, 1.0f);

		// BIND
		glUseProgram(shader);
		glBindVertexArray(vao);
		//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
		ib.Bind();

		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0 );  // 3 porque s�o 3 indices e nao 3 vertices

		/* Swap front and back buffers */
		glfwSwapBuffers(window);

		/* Poll for and process events */
		glfwPollEvents();
	}

	glfwTerminate();
	return 0;
}