#pragma once
#include <GL/glew.h>


class VertexBuffer
{
private:
	unsigned int _bufferID;

public:
	VertexBuffer(unsigned int size, const void *data);
	~VertexBuffer();

	void Bind();
	void Unbind();

};
